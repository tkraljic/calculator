#include <iostream>

int main() {
    float numberOne, numberTwo;
    char operation;

    std::cout << "Calculator" << std::endl;
    std::cin >> numberOne >> operation >> numberTwo;

    float sum = numberOne + numberTwo;
    float difference = numberOne - numberTwo;
    float multiply = numberOne * numberTwo;
    float divide = numberTwo / numberOne;
    float power = pow(numberOne, numberTwo);

    switch(operation)
    {
        case '+':
            std::cout << "Answer: " << sum << std::endl;
            break;
        case '*':
            std::cout << "Answer: " << multiply << std::endl;
            break;
        case '/':
            std::cout << "Answer: " << divide << std::endl;
            break;
        case '^':
            std::cout << "Answer: " << power << std::endl;
            break;
        case '-':
            std::cout << "Answer: " << difference << std::endl;
            break;
        default:
            std::cout << "I don't understand" << std::endl;
            break;
    }

    return 0;
}
